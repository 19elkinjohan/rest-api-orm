package com.elkin.restapiorm.domain.dto;

import lombok.Data;

@Data
public class CityDTO {
    private Long id;
    private String cityName;
}
