package com.elkin.restapiorm.domain.mapper;

import com.elkin.restapiorm.domain.dto.CityDTO;
import com.elkin.restapiorm.domain.entity.City;
import org.springframework.stereotype.Component;

@Component
public class CityMapper implements IMapper<City, CityDTO>{
    @Override
    public CityDTO map(City in) {
        CityDTO responseDTO = new CityDTO();

        responseDTO.setId(in.getId());
        responseDTO.setCityName(in.getCityName());

        return responseDTO;
    }
}
